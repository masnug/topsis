<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends EM_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->restrict(array('admin' => 'all'));
    }

	public function index()
	{
        // $this->set_layout('landing')->render('home/index');
        redirect('home/admin');
    }

    public function admin()
    {
		$this->render('home/admin'); 
    }
}