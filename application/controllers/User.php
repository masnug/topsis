<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends EM_Controller {

	public function __construct() {
		parent::__construct();

		$this->restrict(array(
			'index' => 'all',
			'ganti_password' => 'all',
			'update_password' => 'all',
			'logout' => 'all'
		));
	}

	public function index()
	{
		$this->render('user/index');
	}

	public function ganti_password()
	{
		$this->render('user/ganti_password');
	}
	
	public function update_password()
	{
		$username = $this->session->userdata('userlogin');
		$this->load->model('model_login');
		$rowlogin = $this->model_login->cek($username, $this->input->post('lama'));

		if ($rowlogin) 
		{
			if ($this->input->post('baru') == $this->input->post('konfirmasi')) {
				$data = array(
					'password'=>$this->input->post('baru')
					);
				$this->load->model('model_login');
				$this->model_login->updatedata($data, $username);
				
				redirect(site_url('home/admin'));	
			} else {
				$this->session->set_flashdata('message', 'Password Baru Tidak Sama Dengan Konfirmasi');
				redirect(site_url('user/ganti_password'));		
			}
		}
		else
		{
			$this->session->set_flashdata('message', 'Password Lama Salah');
			redirect(site_url('user/ganti_password'));
		}
	}

	public function login()
	{
		if ($this->user) {
			redirect(site_url('home/admin'));
		}
		
		$this->set_layout('landing')->render('user/login'); 
	}
	
	public function ceklogin() 
	{
		$user = $this->auth($this->input->post('username'), $this->input->post('password'));

		if (! $user) {
			redirect(site_url('user/login'));
		}

		redirect(site_url('home/admin'));
	}
	
	public function logout() 
	{
		$this->session->sess_destroy();

		redirect(site_url('user/login'));
	}
}
