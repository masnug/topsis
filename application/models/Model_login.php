<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model {

	public function getdata()
	{
		return $this->db->get('login')->result();
	}
	
	public function insertdata($data)
	{
		$this->db->insert('login', $data);
		return $this->db->insert_id();
	}
	
	public function deletedata($id)
	{
		$this->db->where('username', $id);
		return $this->db->delete('login');
	}

	public function selectdata($id)
	{
		$this->db->where('username', $id);
		return $this->db->get('login')->row();
	}
	
	public function updatedata($data, $id)
	{
		$this->db->where('username', $id);
		return $this->db->update('login', $data);
	}
	
	public function cek($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		return $this->db->get('login')->row();
	}

}
