(function($) {
    "use strict";

    var mainApp = {
        initFunction: function() {

            $('a[data-confirm]').click(function(e) {
                var msg = $(this).data('confirm');

                return confirm(msg);
            });

            $('.datatables table').DataTable({
                "language": {
                    "lengthMenu": "Menampilkan _MENU_ rekam per halaman",
                    "zeroRecords": "Maaf, tidak menemukan data.",
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Tidak tersedia rekam",
                    "infoFiltered": "(menyaring dari total _MAX_ rekam)",
                    "paginate": {
                        "first":      "Pertama",
                        "last":       "Terakhir",
                        "next":       "Lanjut",
                        "previous":   "Kembali"
                    },
                    "search":         "Cari: ",
                }
            });

            $('[data-print-element]').click(function(e) {
                e.preventDefault();

                var target = $(this).data('printElement');
                var html = $(target).html();
                var printWindow = window.open('', 'new div', 'height=450,width=800');

                printWindow.document.write('<html><head><title>Cetak Penrankingan</title>');
                printWindow.document.write('<style>* {font-family: sans-serif;} table {width: 100%;} table thead * {text-align: left;} h1 {font-size: 26px;} h2 {font-size: 16px;}</style>');
                printWindow.document.write('</head><body>');
                printWindow.document.write(html);
                printWindow.document.write('</body></html>');

                printWindow.print();
                printWindow.close();
            })
            
        },

        initialization: function () {
            mainApp.initFunction();
        }

    }

    // Initializing ///
    $(document).ready(function () {
        mainApp.initFunction();
    });

}(jQuery)); 