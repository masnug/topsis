<div class="row">
    <div class="col-md-12">
      <h1 class="page-header">Kriteria <small>Tabel Data</small></h1>
      <div class="panel panel-default">
        <div class="panel-heading">Data Kriteria</div>
        <div class="panel-body">
          <div class="table-responsive">
            <div class="clearfix" style="margin-bottom: 10px;">
              <a class="btn btn-primary pull-right" href="<?php echo site_url('kriteria/add') ?>"><i class="fa fa-plus-circle"></i> Tambah Kriteria</a>
            </div>
            <table class="table table-striped table-bordered table-hover no-footer">
              <thead>
                <tr role="row">
                  <th colspan="1" rowspan="1" style="width: 50px;" tabindex="0" class="text-center">No.</th>
                  <th colspan="1" rowspan="1" style="width: 50px;" tabindex="0" class="text-center">ID</th>
                  <th class="sorting" colspan="1" rowspan="1" tabindex="0">Nama Kriteria</th>
                  <th class="sorting" colspan="1" rowspan="1" tabindex="0">Tingkat Kepentingan</th>
                  <th class="sorting" colspan="1" rowspan="1" tabindex="0">Biaya / Keuntungan</th>
                  <th class="sorting" colspan="1" rowspan="1" style="width: 100px;" tabindex="0">&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                <?php $start = $offset + 1 ?>
                <?php foreach ($tabelkriteria as $i => $rowkriteria): ?>
                  <tr class="<?php echo $i % 2 ? 'odd' : 'even' ?>">
                    <td class="text-center"><?php echo $start++; ?></td>
                    <td class="text-center"><?php echo $rowkriteria->id_kriteria; ?></td>
                    <td><?php echo $rowkriteria->nama_kriteria; ?></td>
                    <td><?php echo $rowkriteria->kepentingan; ?></td>
                    <td><?php echo $rowkriteria->costbenefit == 'cost' ? 'Biaya' : 'Keuntungan'; ?></td>
                    <td class="text-center">
                      <a href="<?php echo site_url('kriteria/edit/'.$rowkriteria->id_kriteria); ?>"
                        class="btn btn-default btn-xs"
                        data-toggle="tooltip"
                        data-placement="bottom"
                        title="Edit"
                      >
                        <i class="fa fa-pencil"></i>
                      </a>
                      <a href="<?php echo site_url('kriteria/delete/'.$rowkriteria->id_kriteria); ?>"
                        class="btn btn-danger btn-xs"
                        data-toggle="tooltip"
                        data-placement="bottom"
                        title="Hapus"
                        data-confirm="Anda akan menghapus <?php echo $rowkriteria->nama_kriteria ?>. Lanjut?"
                      >
                        <i class="fa fa-trash-o"></i>
                      </a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <div class="row">
              <div class="col-sm-12">
                <?php echo $pagination ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>