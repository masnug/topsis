<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alternatif extends EM_Controller {

	public function __construct() {
		parent::__construct();

		$this->restrict(array(
			'index' => 'admin',
			'add' => 'admin',
			'save' => 'admin',
			'delete' => 'admin',
			'edit' => 'admin',
			'update' => 'admin',
		));
	}

	public function index($offset = 0)
	{
		$this->load->model('model_alternatif');
		$this->load->model('model_kriteria');

		$data['tabelalternatif'] = $this->model_alternatif->getdata($offset, $this->input->post('q'));
		$data['kriteria_tersedia'] = $this->model_kriteria->getdata();
		$data['pagination'] = $this->paginator();
		$data['offset'] = $offset;

		$this->render('alternatif/index', $data);
	}
	
	public function add()
	{
		$this->load->model('model_kriteria');
		$data['kriteria_tersedia'] = $this->model_kriteria->getdata();

		$this->render('alternatif/add', $data);
	}

	public function save()
	{
		$data = array(
			'nama_alternatif'=>$this->input->post('nama_alternatif'),
			'deskripsi'=>$this->input->post('deskripsi')
		);

		$this->load->model('model_alternatif');
		$this->model_alternatif->insertdata($data, $this->input->post('nilai'));
		
		redirect(site_url('alternatif'));
	}

	public function delete()
	{
		$id = $this->uri->segment(3);

		$this->load->model('model_alternatif');
		$this->model_alternatif->deletedata($id);
		
		redirect(site_url('alternatif'));
	}
	
	public function edit() 
	{
		$this->load->model('model_alternatif');
		$this->load->model('model_kriteria');

		$id = $this->uri->segment(3);
		$data['rowalternatif'] = $this->model_alternatif->selectdata($id);
		$data['kriteria_tersedia'] = $this->model_kriteria->getdata();

		$this->render('alternatif/edit', $data);
	}
	
	public function update()
	{
		$id = $this->uri->segment(3);
		$data = array(
			'nama_alternatif'=>$this->input->post('nama_alternatif'),
			'deskripsi'=>$this->input->post('deskripsi')
			);

		$this->load->model('model_alternatif');
		$this->model_alternatif->updatedata($data, $id, $this->input->post('nilai'));
		
		redirect(site_url('alternatif'));	
	}

	protected function paginator() {
		$config['base_url'] = site_url('alternatif/index');
		$config['total_rows'] = $this->model_alternatif->countdata();
		$config['uri_segment'] = 3;
		$config['per_page'] = 20;

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		return $this->pagination->create_links();
	}
}
