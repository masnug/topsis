<div class="row">
  <div class="col-md-12">
    <h1 class="page-header">Alternatif <small>Edit Data</small></h1>
    <div class="panel panel-default">
      <div class="panel-heading">Data Alternatif</div>
      <div class="panel-body">
        <form method="post" action="<?php echo site_url('alternatif/update/'.$rowalternatif->id_alternatif); ?>" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-3 control-label" for="nama">Nama Alternatif Produk</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nama" placeholder="Nama alternatif" name="nama_alternatif" value="<?php echo $rowalternatif->nama_alternatif; ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="deskripsi">Deskripsi</label>
            <div class="col-sm-9">
              <textarea name="deskripsi" id="deskripsi" class="form-control" rows="3" placeholder="Deskripsi produk"><?php echo $rowalternatif->deskripsi; ?></textarea>
            </div>
          </div>

          <fieldset>
            <legend>Kriteria</legend>
            <?php foreach ($kriteria_tersedia as $kriteria): ?>
              <div class="form-group">
                <label class="col-sm-3 control-label" for="nama"><?php echo $kriteria->nama_kriteria ?></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="nama" placeholder="1 .. 100" name="nilai[<?php echo $kriteria->id_kriteria ?>]" value="<?php echo isset($rowalternatif->kriteria[$kriteria->id_kriteria]) ? $rowalternatif->kriteria[$kriteria->id_kriteria] : '' ?>">
                </div>
              </div>
            <?php endforeach ?>
          </fieldset>

          <div class="form-group">  
            <div class="col-sm-offset-3 col-sm-9">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <a href="<?php echo site_url('alternatif') ?>" class="btn btn-link">Batal</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>