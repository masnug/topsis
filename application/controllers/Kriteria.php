<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria extends EM_Controller {

	public function __construct() {
		parent::__construct();

		$this->restrict(array(
			'index' => 'executive',
			'add' => 'executive',
			'save' => 'executive',
			'delete' => 'executive',
			'edit' => 'executive',
			'update' => 'executive',
		));
	}

	public function index($offset = 0)
	{
		$this->load->model('model_kriteria');

		$data['tabelkriteria'] = $this->model_kriteria->getdata($offset);
		$data['pagination'] = $this->paginator();
		$data['offset'] = $offset;

		$this->render('kriteria/index', $data);
	}
	
	public function add()
	{
		$this->render('kriteria/add');
	}

	public function save()
	{
		$data = array(
			'nama_kriteria'=>$this->input->post('nama_kriteria'),
			'kepentingan'=>$this->input->post('kepentingan'),
			'costbenefit'=>$this->input->post('costbenefit')
			);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama_kriteria', 'Nama Kriteria', 'required');
        $this->form_validation->set_rules('kepentingan', 'Tingkat kepentingan', 'required|greater_than_equal_to[1]|less_than_equal_to[5]');
        $this->form_validation->set_rules('costbenefit', 'Biaya / Keutungan', 'required');

        if (! $this->form_validation->run()) {
			$this->session->set_flashdata('message', 'Isian formulir tidak benar, silakan perbaiki isian.');

			redirect(site_url('kriteria/add'));
        }
        else {
			$this->load->model('model_kriteria');
			$this->model_kriteria->insertdata($data);
			
			redirect(site_url('kriteria'));
        }
	}

	public function delete()
	{
		$id = $this->uri->segment(3);
		$this->load->model('model_kriteria');
		$this->model_kriteria->deletedata($id);
		
		redirect(site_url('kriteria'));
	}
	
	public function edit() 
	{
		$id = $this->uri->segment(3);

        $this->load->library('form_validation');
		$this->load->model('model_kriteria');
		
		$data['rowkriteria'] = $this->model_kriteria->selectdata($id);

		$this->render('kriteria/edit', $data);
	}
	
	public function update()
	{
		$id = $this->uri->segment(3);
		$data = array(
			'nama_kriteria'=>$this->input->post('nama_kriteria'),
			'kepentingan'=>$this->input->post('kepentingan'),
			'costbenefit'=>$this->input->post('costbenefit')
			);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama_kriteria', 'Nama Kriteria', 'required');
        $this->form_validation->set_rules('kepentingan', 'Tingkat kepentingan', 'required|greater_than_equal_to[1]|less_than_equal_to[5]');
        $this->form_validation->set_rules('costbenefit', 'Biaya / Keutungan', 'required');

        if (! $this->form_validation->run()) {
			$this->session->set_flashdata('message', 'Isian formulir tidak benar, silakan perbaiki isian.');

			redirect(site_url('kriteria/edit/'.$id));
        }
        else {
			$this->load->model('model_kriteria');
			$this->model_kriteria->updatedata($data, $id);
			
			redirect(site_url('kriteria'));	
        }
        
	}

    protected function paginator() {
        $config['base_url'] = site_url('kriteria/index');
        $config['total_rows'] = $this->model_kriteria->countdata();
        $config['uri_segment'] = 4;
        $config['per_page'] = 20;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }
}
