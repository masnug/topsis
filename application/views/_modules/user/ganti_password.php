<div class="row">
  <div class="col-md-12">
    <h1 class="page-header">
      User <small>Ganti Password</small>
    </h1>

    <div class="panel panel-default">
      <div class="panel-heading">Informasi Akun</div>
      <div class="panel-body">
        <form method="post" action="<?php echo site_url('user/update_password'); ?>" class="form-horizontal">
          <div class="form-group">
            <label for="username" class="control-label col-sm-3">Username</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="username" id="username" value="<?php echo $this->session->userdata('userlogin'); ?>" readonly="readonly">
            </div>
          </div>
          <div class="form-group">
            <label for="old" class="control-label col-sm-3">Password Lama</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="lama" id="old">
            </div>
          </div>
          <div class="form-group">
            <label for="new" class="control-label col-sm-3">Password Baru</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="baru" id="new">
            </div>
          </div>
          <div class="form-group">
            <label for="confirm" class="control-label col-sm-3">Konfirmasi Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="konfirmasi" id="confirm">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <button type="submit" class="btn btn-primary">Perbaharui</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>