<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Hasil Analisa <small>SPK Metode Topsis</small>
        </h1>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                Hasil Analisa Menggunakan SPK Metode Topsis
            </div>
            <div class="panel-body">
                <div class="alert alert-success">
                    Nama KWT Terbaik <strong><?php echo $alternatifrangking[0]; ?></strong> dengan nilai <strong><?php echo number_format($hasilrangking[0], 2, ',', '.'); ?></strong>
                </div>
                
                <button class="btn btn-default pull-right" data-print-element="#print-this-out"><i class="fa fa-print"></i> Cetak</button>
                <h4>Perankingan</h4>
                <hr>
                <div class="table-responsive datatables">
                    <?php ob_start() ?>
                        <table class="table table-striped">
                          <thead>
                            <tr>
                                <th>Ranking</th>
                                <th>Nama KWT</th>
                                <th>Alamat</th>
                                <th>Nilai</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php for ($i=0;$i<count($hasilrangking);$i++):?>
                                <tr>
                                    <td><?php echo ($i+1); ?></td>
                                    <td><?php echo $alternatifrangking[$i]; ?></td>
                                    <td><?php echo $deskripsi[$i]; ?></td>
                                    <td><?php echo number_format($hasilrangking[$i], 2, ',', '.'); ?></td>
                                </tr>
                            <?php endfor; ?>
                          </tbody>
                        </table>
                    <?php $table = ob_get_contents(); ob_get_flush() ?>
                </div>

                <div id="print-this-out" style="display: none;">
                    <h1>Hasil Analisa Menggunakan SPK Metode Topsis</h1>
                    <hr>
                    <h2>Perankingan</h2>
                    <?php echo $table; ?>
                </div>
            </div>
        </div>
    </div>
</div>