-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 23, 2016 at 04:59 
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_topsis_ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `alternatif`
--

CREATE TABLE IF NOT EXISTS `alternatif` (
  `id_alternatif` int(11) NOT NULL AUTO_INCREMENT,
  `nama_alternatif` varchar(200) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id_alternatif`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `alternatif`
--

INSERT INTO `alternatif` (`id_alternatif`, `nama_alternatif`, `deskripsi`) VALUES
(9, 'Margomulyo', 'Kembang, Melikan'),
(10, 'Sedio Mulyo', 'Kendal, Melikan'),
(5, 'Ngudi Makmur', 'Ngebang wetan, Melikan\r\n'),
(6, 'Sidomakmur', 'Ngampiran, Melikan'),
(7, 'Sidomulyo', 'Ngricik, Melikan'),
(8, 'Budi Usodo', 'Tambak, Melikan'),
(11, 'Bimaram', 'Banombo b, Pucanganom'),
(12, 'Lestari', 'Pucanganom C, Pucanganom'),
(13, 'Maju Lestari', 'Janglot, Pucanganom'),
(14, 'Mawar', 'Bamban Bohol');

-- --------------------------------------------------------

--
-- Table structure for table `alternatif_kriteria`
--

CREATE TABLE IF NOT EXISTS `alternatif_kriteria` (
  `id_alternatif_kriteria` int(11) NOT NULL AUTO_INCREMENT,
  `id_alternatif` int(11) DEFAULT NULL,
  `id_kriteria` int(11) DEFAULT NULL,
  `nilai` double DEFAULT NULL,
  PRIMARY KEY (`id_alternatif_kriteria`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `alternatif_kriteria`
--

INSERT INTO `alternatif_kriteria` (`id_alternatif_kriteria`, `id_alternatif`, `id_kriteria`, `nilai`) VALUES
(33, 10, 3, 8),
(32, 10, 2, 20),
(28, 6, 1, 12),
(31, 10, 1, 12),
(29, 6, 2, 20),
(30, 6, 3, 7),
(26, 9, 2, 20),
(27, 10, 3, 5),
(25, 9, 1, 12),
(34, 8, 1, 10),
(35, 8, 2, 20),
(36, 8, 3, 20),
(37, 9, 1, 12),
(38, 9, 2, 20),
(39, 9, 3, 3),
(40, 7, 1, 12),
(41, 7, 2, 25),
(42, 7, 3, 10),
(43, 11, 1, 10),
(44, 11, 2, 15),
(45, 11, 3, 7),
(46, 12, 1, 11),
(47, 12, 2, 20),
(48, 12, 3, 3),
(49, 13, 1, 9),
(50, 13, 2, 20),
(51, 13, 3, 5),
(52, 14, 1, 12),
(53, 14, 2, 24),
(54, 13, 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE IF NOT EXISTS `kriteria` (
  `id_kriteria` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(100) DEFAULT NULL,
  `kepentingan` double DEFAULT NULL,
  `costbenefit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kriteria`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `nama_kriteria`, `kepentingan`, `costbenefit`) VALUES
(1, 'Jumlah Pertemuan', 2, 'benefit'),
(2, 'Kepengurusan', 4, 'benefit'),
(3, 'Jumlah Kegiatan', 5, 'benefit');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`) VALUES
('admin', '123');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
