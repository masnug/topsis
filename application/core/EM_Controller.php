<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

abstract class EM_Controller extends CI_Controller {

    protected $layouts_view_dir = '_layouts';

    protected $modules_view_dir = '_modules';

    protected $layout = 'default';

    protected $vars = array();

    public $user;

    public $logged_in = false;

    public function __construct()
    {
        parent::__construct();

        $this->check_user();
    }

    protected function set_layout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    protected function render($view, $vars = array())
    {
        $layout = $this->layouts_view_dir.DIRECTORY_SEPARATOR.$this->layout;
        $page_view = $this->modules_view_dir.DIRECTORY_SEPARATOR.$view;
        $vars = array_merge($this->vars, $vars, ['page_view' => $page_view]);

        $this->load->view($layout, $vars);
    }

    public function auth($username, $password)
    {
        $this->load->model('model_login');

        $rowlogin = $this->model_login->cek($username, $password);

        if ($rowlogin) {
            $this->session->set_userdata('userlogin', $rowlogin->username);
            $this->session->set_userdata('role', $rowlogin->role);
        }
        else {
            $this->session->set_flashdata('message', 'Kombinasi username & password tidak ditemukan!');
        }

        return $rowlogin;
    }

    public function check_user()
    {
        $username = $this->session->userdata('userlogin');

        if ($username) {
            $this->user = new stdClass();
            $this->user->username = $username;
            $this->user->role = $this->session->userdata('role');

            $this->logged_in = true;
        }
    }

    /**
     * pembatasan akses aksi kontroller
     * @param  mixed $restriction_rules  'all': untuk semua aksi dibatasi hanya user login boleh akses
     *                                   ['index' => 'all']: hanya aksi index yang dibatasi
     *                                   ['index' => 'executive']: hanya role executive yg boleh akses aksi index
     *                                  
     * @return [type]                    [description]
     */
    protected function restrict($restriction_rules = 'all')
    {
        $restricted = false;
        $method = $this->router->method;

        // periksa aturan pembatasan akses
        if ($restriction_rules == 'all' || isset($restriction_rules[$method])) {
            $restricted = true;
        }
        
        // jika tidak dibatasi
        if (! $restricted) {
            return;
        }

        // klo lom, ya login dulu
        if (! $this->logged_in) {
            $this->session->set_flashdata('message', 'Anda belum login!');
            
            redirect(site_url('user/login'));
        }

        // jika tidak ada aturan pembatasan peran lanjut
        if ($restriction_rules == 'all') {
            return;
        }

        // periksa peran yg diperbolehkan
        $allowed_roles = $restriction_rules[$method];

        // jika boleh semua peran, lanjut
        if ($allowed_roles == 'all') {
            return;
        }

        $allowed_roles = (array)$allowed_roles;

        // jika role tidak ada, tendang
        if (! in_array($this->user->role, $allowed_roles)) {
            $this->session->set_flashdata('message', 'Anda tidak memiliki cukup hak untuk mengakses halaman <strong>'.uri_string().'</strong>!');
            
            redirect(site_url('home/admin'));
        }
    }
}