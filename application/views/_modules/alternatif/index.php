<div class="row">
    <div class="col-md-12">
      <h1 class="page-header">Alternatif <small>Tabel Data</small></h1>
      <div class="panel panel-default">
        <div class="panel-heading">Data Alternatif</div>
        <div class="panel-body">
          <div class="table-responsive">
            <div class="clearfix row" style="margin-bottom: 10px;">
              <div class="col-sm-6">
                <form action="<?php echo site_url('alternatif') ?>" method="post">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cari nama atau alamat..." name="q">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Cari</button>
                    </span>
                  </div>
                </form>
              </div>
              <div class="col-sm-6">
                <a class="btn btn-primary pull-right" href="<?php echo site_url('alternatif/add') ?>"><i class="fa fa-plus-circle"></i> Tambah Alternatif</a>
              </div>
            </div>
            <table class="table table-striped table-bordered table-hover no-footer">
              <thead>
                <tr role="row">
                  <th colspan="1" rowspan="1" style="width: 50px;" tabindex="0" class="text-center">No.</th>
                  <th colspan="1" rowspan="1" style="width: 50px;" tabindex="0" class="text-center">ID</th>
                  <th class="sorting" colspan="1" rowspan="1" tabindex="0">Nama KWT</th>
                  <th class="sorting" colspan="1" rowspan="1" tabindex="0">Alamat</th>
                  <th class="sorting" colspan="<?php echo count($kriteria_tersedia) ?>" rowspan="1" tabindex="0">Kriteria</th>
                  <th class="sorting" colspan="1" rowspan="1" style="width: 100px;" tabindex="0">&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                <?php $start = $offset + 1; ?>
                <?php foreach ($tabelalternatif as $i => $rowalternatif): ?>
                  <tr class="<?php echo $i % 2 ? 'odd' : 'even' ?>">
                    <td class="text-center"><?php echo $start++; ?></td>
                    <td class="text-center"><?php echo $rowalternatif->id_alternatif; ?></td>
                    <td><?php echo $rowalternatif->nama_alternatif; ?></td>
                    <td><?php echo $rowalternatif->deskripsi; ?></td>
                    <?php foreach ($kriteria_tersedia as $kriteria): ?>
                      <td class="text-center">
                        <?php echo isset($rowalternatif->kriteria[$kriteria->id_kriteria]) ? $rowalternatif->kriteria[$kriteria->id_kriteria] : '-'; ?>
                      </td>
                    <?php endforeach ?>
                    <td class="text-center">
                      <a href="<?php echo site_url('alternatif/edit/'.$rowalternatif->id_alternatif); ?>"
                        class="btn btn-default btn-xs"
                        data-toggle="tooltip"
                        data-placement="bottom"
                        title="Edit"
                      >
                        <i class="fa fa-pencil"></i>
                      </a>
                      <a href="<?php echo site_url('alternatif/delete/'.$rowalternatif->id_alternatif); ?>"
                        class="btn btn-danger btn-xs"
                        data-toggle="tooltip"
                        data-placement="bottom"
                        title="Hapus"
                        data-confirm="Anda akan menghapus <?php echo $rowalternatif->nama_alternatif ?>. Lanjut?"
                      >
                        <i class="fa fa-trash-o"></i>
                      </a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <div class="row">
              <div class="col-sm-12">
                <?php echo $pagination ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>