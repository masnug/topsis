<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_alternatif_kriteria extends CI_Model {

	public function getdata($offset)
	{
		$this->db->select('*')
			->from('alternatif_kriteria')
			->join('alternatif', 'alternatif.id_alternatif=alternatif_kriteria.id_alternatif', 'left')
			->join('kriteria', 'kriteria.id_kriteria=alternatif_kriteria.id_kriteria', 'left');

		if ($offset) {
			$this->db->limit($offset, 20);
		}
		else {
			$this->db->limit(20);
		}

		$retval = $this->db->get()->result();

		return $retval;
	}

	public function countdata()
	{
		return $this->db->from('alternatif_kriteria')
			->join('alternatif', 'alternatif.id_alternatif=alternatif_kriteria.id_alternatif', 'left')
			->join('kriteria', 'kriteria.id_kriteria=alternatif_kriteria.id_kriteria', 'left')
			->count_all_results();
	}
	
	public function insertdata($data)
	{
		return $this->db->insert('alternatif_kriteria', $data);
	}
	
	public function deletedata($id)
	{
		$this->db->where('id_alternatif_kriteria', $id);
		return $this->db->delete('alternatif_kriteria');
	}

	public function selectdata($id)
	{
		$this->db->where('id_alternatif_kriteria', $id);
		return $this->db->get('alternatif_kriteria')->row();
	}

	public function select_alternatif_kriteria($id_alternatif, $id_kriteria)
	{
		$this->db->where('id_alternatif', $id_alternatif);
		$this->db->where('id_kriteria', $id_kriteria);
		
		return $this->db->get('alternatif_kriteria')->row();
	}
	
	public function updatedata($data, $id)
	{
		$this->db->where('id_alternatif_kriteria', $id);
		return $this->db->update('alternatif_kriteria', $data);
	}

}
