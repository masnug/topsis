<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>SPK Metode Topsis</title>
  <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url('assets/js/dataTables/dataTables.bootstrap.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/custom-styles.css') ?>" rel="stylesheet">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
      <div class="navbar-header">
        <button class="navbar-toggle" data-target=".sidebar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="<?php echo site_url('home') ?>">SPK Topsis</a>
      </div>
      <ul class="nav navbar-top-links navbar-right">
        <!-- /.dropdown -->
        <li class="dropdown">
          <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#" style="text-transform: capitalize;"><i class="fa fa-user fa-fw"></i> <?php echo $this->user->username; ?> <i class="fa fa-caret-down"></i></a>
          <ul class="dropdown-menu dropdown-user">
            <li>
              <a href="<?php echo site_url('user/ganti_password'); ?>"><i class="fa fa-pencil-square-o fa-fw"></i> Ganti Password</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="<?php echo site_url('user/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
          </ul><!-- /.dropdown-user -->
        </li><!-- /.dropdown -->
      </ul>
    </nav><!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li>
            <a href="<?php echo site_url('home/admin'); ?>"><i class="fa fa-dashboard"></i> Home</a>
          </li>
          <?php if ($this->user->role == 'executive'): ?>
            <li>
              <a href="<?php echo site_url('kriteria'); ?>"><i class="fa fa-list-ol"></i> Kriteria</a>
            </li>
          <?php endif ?>
          <?php if ($this->user->role == 'admin'): ?>
            <li>
              <a href="<?php echo site_url('alternatif'); ?>"><i class="fa fa-book"></i> Alternatif</a>
            </li>
          <?php endif ?>
          <li>
            <a href="<?php echo site_url('topsis'); ?>"><i class="fa fa-file-text-o"></i> Hasil Analisa</a>
          </li>
        </ul>
      </div>
    </nav><!-- /. NAV SIDE  -->
    <div id="page-wrapper">
      <div id="page-inner">
        <?php $message = @$this->session->flashdata('message'); ?>

        <?php if ($message): ?>
          <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <div class="alert alert-danger">
                  <?php echo $message ?>
                </div>
              </div>
            </div>  
        <?php endif ?>

        <?php $this->load->view($page_view); ?>

        <footer>
          <p>All right reserved. Template by: <a href="http://webthemez.com">WebThemez</a></p>
        </footer>
      </div><!-- /. PAGE INNER  -->
    </div><!-- /. PAGE WRAPPER  -->
  </div><!-- /. WRAPPER  -->
  <!-- JS Scripts-->
  <script src="<?php echo base_url('assets/js/jquery-1.10.2.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script> 
  <script src="<?php echo base_url('assets/js/dataTables/jquery.dataTables.js') ?>"></script> 
  <script src="<?php echo base_url('assets/js/dataTables/dataTables.bootstrap.js') ?>"></script> 
  <script src="<?php echo base_url('assets/js/app.js') ?>"></script>
</body>
</html>