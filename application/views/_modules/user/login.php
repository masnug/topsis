<div>
  <div class="row">
    <div class="col-sm-6 col-sm-offset-1">
      <div class="well well-sm">
        <h4 class="text-center">Tentang BP2KP</h4>
        <hr>
        <img src="<?php echo base_url('assets/img/logo.png') ?>" alt="Logo" style="float:left;padding:30px 30px 10px 10px;width:150px;">
        <p>
          <strong>Badan Pelaksana Penyuluhan dan Ketahanan Pangan (BP2KP) Kabupaten Gunungkidul</strong> dibentuk dan ditetapkan berdasarkan Peraturan Daerah Kabupaten Gunungkidul Nomor 12 Tahun 2008 tentang Pembentukan, Susunan Organisasi, Kedudukan, dan Tugas Lembaga Teknis Daerah yang untuk selanjutnya telah disusun Peraturan Bupati Gunungkidul Nomor 199 Tahun 2008 tentang Uraian Tugas Badan Pelaksana Penyuluhan dan Ketahanan Pangan.
        </p>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="well well-sm">
        <h4 class="text-center">Login :: SPK Topsis</h4>
        <hr>
        <form action="<?php echo site_url('user/ceklogin'); ?>" method="post" class="form">
          <div class="form-group">
            <label for="">Username</label>
            <input class="form-control" type="text" name="username" id="username" placeholder="admin" />
          </div>        
          <div class="form-group">
            <label for="">Password</label>
            <input class="form-control" type="password" name="password" id="password" placeholder="123" />
          </div>          

          <div class="text-center clearfix" style="margin-top: 40px;">
            <button type="submit" class="btn btn-primary btn-block btn-lg">Login</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>