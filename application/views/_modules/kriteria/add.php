<div class="row">
  <div class="col-md-12">
    <h1 class="page-header">Kriteria <small>Tambah Data</small></h1>
    <div class="panel panel-default">
      <div class="panel-heading">Data Kriteria</div>
      <div class="panel-body">
        <form method="post" action="<?php echo site_url('kriteria/save'); ?>" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-3 control-label" for="nama">Nama Kriteria</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nama" placeholder="Nama kriteria" name="nama_kriteria">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="kepentingan">Tingkat Kepentingan</label>
            <div class="col-sm-9">
              <select name="kepentingan" class="form-control" id="kepentingan" placeholder="Pilih 1 s/d 5">
                <option value="">Pilih 1 s/d 5</option>
                <option value="1">1. Tidak Penting</option>
                <option value="2">2. Agak Penting</option>
                <option value="3">3. Cukup Penting</option>
                <option value="4">4. Penting</option>
                <option value="5">5. Sangat Penting</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="exampleInputFile">Biaya / Keuntungan</label>
            <div class="col-sm-9">
              <label class="radio-inline" id="inlineRadio1">
                <input type="radio" name="costbenefit" id="inlineRadio1" value="cost"> Biaya
              </label>
              <label class="radio-inline" id="inlineRadio1">
                <input type="radio" name="costbenefit" id="inlineRadio2" value="benefit"> Keuntungan
              </label>
            </div>
          </div>
          <div class="form-group">  
            <div class="col-sm-offset-3 col-sm-9">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <a href="<?php echo site_url('kriteria') ?>" class="btn btn-link">Batal</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>