<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_alternatif extends CI_Model {

	public function getdata($offset = false, $q = null)
	{
		$this->db->from('alternatif')
			->order_by('id_alternatif');

		if ($q) {
			$this->db->like('nama_alternatif', $q)
				->or_like('deskripsi', $q);
		}

		if ($offset !== false) {
			if ($offset > 0) {
				$this->db->limit($offset, 20);
			}
			else {
				$this->db->limit(20);
			}
		}

		$result = $this->db->get()->result();

		foreach ($result as $row) {
			$row->kriteria = array();
			$kriteria_rows = $this->db->from('alternatif_kriteria')
				->where('id_alternatif', $row->id_alternatif)
				->order_by('id_kriteria')
				->get()
				->result();

			foreach ($kriteria_rows as $kriteria) {
				$row->kriteria[$kriteria->id_kriteria] = $kriteria->nilai;
			}
		}

		return $result;
	}

	public function countdata()
	{
		return $this->db->from('alternatif')->count_all_results();
	}
	
	public function insertdata($data, $nilai_arr)
	{
		$alternatif = $this->db->insert('alternatif', $data);
		$id_alternatif = $this->db->insert_id();

		$this->sync_nilai_kriteria($id_alternatif, $nilai_arr);

		return $alternatif;
	}

	public function sync_nilai_kriteria($id_alternatif, $nilai_arr)
	{
		foreach ($nilai_arr as $id_kriteria => $nilai) {
			// cek data ada ato tidak
			$ada = $this->db->where('id_alternatif', $id_alternatif)
				->where('id_kriteria', $id_kriteria)
				->count_all_results('alternatif_kriteria');

			if ($ada) {
				$this->db->where('id_alternatif', $id_alternatif)
					->where('id_kriteria', $id_kriteria)
					->update('alternatif_kriteria', compact('nilai'));
			}
			else {
				$this->db->insert('alternatif_kriteria', compact('id_alternatif', 'id_kriteria', 'nilai'));
			}
		}

	}
	
	public function deletedata($id)
	{
		$this->db->where('id_alternatif', $id);
		return $this->db->delete('alternatif');
	}

	public function selectdata($id)
	{
		$this->db->where('id_alternatif', $id);

		$row = $this->db->get('alternatif')->row();
		$row->kriteria = array();

		$kriteria_rows = $this->db->from('alternatif_kriteria')
			->where('id_alternatif', $row->id_alternatif)
			->order_by('id_kriteria')
			->get()
			->result();

		foreach ($kriteria_rows as $kriteria) {
			$row->kriteria[$kriteria->id_kriteria] = $kriteria->nilai;
		}

		return $row;
	}
	
	public function updatedata($data, $id, $nilai_arr)
	{
		$this->db->where('id_alternatif', $id);
		$res = $this->db->update('alternatif', $data);

		if ($res) {
			$this->sync_nilai_kriteria($id, $nilai_arr);
		}

		return false;
	}

}
