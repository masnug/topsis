<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Alternatif_Kriteria extends EM_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->restrict('all');
    }

    public function index($offset = 0)
    {
        $this->load->model('model_alternatif_kriteria');

        $data['tabelalternatifkriteria'] = $this->model_alternatif_kriteria->getdata($offset);
        $data['pagination'] = $this->paginator();

        $this->render('alternatif_kriteria/index', $data);
    }

    public function add()
    {
        $this->load->model('model_alternatif');
        $this->load->model('model_kriteria');

        $data['tabelalternatif'] = $this->model_alternatif->getdata();
        $data['tabelkriteria'] = $this->model_kriteria->getdata();

        $this->render('alternatif_kriteria/add', $data);
    }

    public function save()
    {
        $this->load->model('model_alternatif_kriteria');

        $data = array(
            'id_alternatif' => $this->input->post('id_alternatif'),
            'id_kriteria' => $this->input->post('id_kriteria'),
            'nilai' => $this->input->post('nilai'),
        );

        $this->model_alternatif_kriteria->insertdata($data);

        redirect(site_url('alternatif_kriteria'));
    }

    public function delete()
    {
        $id = $this->uri->segment(3);

        $this->load->model('model_alternatif_kriteria');
        $this->model_alternatif_kriteria->deletedata($id);

        redirect(site_url('alternatif_kriteria'));
    }

    public function edit()
    {
        $id = $this->uri->segment(3);
        
        $this->load->model('model_alternatif');
        $this->load->model('model_kriteria');
        $this->load->model('model_alternatif_kriteria');
        
        $data['tabelalternatif'] = $this->model_alternatif->getdata();
        $data['tabelkriteria'] = $this->model_kriteria->getdata();
        $data['rowalternatifkriteria'] = $this->model_alternatif_kriteria->selectdata($id);

        $this->render('alternatif_kriteria/edit', $data);
    }

    public function update()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'id_alternatif' => $this->input->post('id_alternatif'),
            'id_kriteria' => $this->input->post('id_kriteria'),
            'nilai' => $this->input->post('nilai'),
            );

        $this->load->model('model_alternatif_kriteria');
        $this->model_alternatif_kriteria->updatedata($data, $id);

        redirect(site_url('alternatif_kriteria'));
    }

    protected function paginator()
    {
        $config['base_url'] = site_url('alternatif_kriteria/index');
        $config['total_rows'] = $this->model_alternatif_kriteria->countdata();
        $config['uri_segment'] = 3;
        $config['per_page'] = 20;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }
}
