<a href="<?php echo site_url('home'); ?>">Kembali</a><br />
<style type="text/css">
<!--
body,td,th {
	font-family: Georgia, Times New Roman, Times, serif;
	font-size: 13px;
	color: #333333;
}
-->
</style>
<?php 
	function tampiltabel($arr)
	{
		echo '<table width="500" border="0" cellspacing="1" cellpadding="3" bgcolor="#000099">';
		  for ($i=0;$i<count($arr);$i++)
		  {
		  echo '<tr>';
			  for ($j=0;$j<count($arr[$i]);$j++)
			  {
			    echo '<td bgcolor="#FFFFFF">'.$arr[$i][$j].'</td>';
			  }
		  echo '</tr>';
		  }
		echo '</table>';
	}

	function tampilbaris($arr)
	{
		echo '<table width="500" border="0" cellspacing="1" cellpadding="3" bgcolor="#000099">';
		echo '<tr>';
			  for ($i=0;$i<count($arr);$i++)
			  {
			    echo '<td bgcolor="#FFFFFF">'.$arr[$i].'</td>';
			  }
		echo "</tr>";
		echo '</table>';
	}

	function tampilkolom($arr)
	{
		echo '<table width="500" border="0" cellspacing="1" cellpadding="3" bgcolor="#000099">';
	  for ($i=0;$i<count($arr);$i++)
	  {
			echo '<tr>';
			    echo '<td bgcolor="#FFFFFF">'.$arr[$i].'</td>';
			echo "</tr>";
	   }
		echo '</table>';
	}
	
	$alternatif = array("Galaxy", "iPhone", "BB", "Lumia");
	$kriteria = array("Harga", "Kualitas", "Fitur", "Populer", "Purna Jual", "Keawetan");
	$costbenefit = array("cost", "benefit", "benefit", "benefit", "benefit", "benefit");
	$kepentingan = array(4, 5, 4, 3, 3, 2);
	$alternatifkriteria = array(
							array(3500, 70, 10, 80, 3000, 36),											
							array(4500, 90, 10, 60, 2500, 48),					                           
							array(4000, 80, 9, 90, 2000, 48),												                            
							array(4000, 70, 8, 50, 1500, 60)
						  ); 
	
	$pembagi = array();
	
	for ($i=0;$i<count($kriteria);$i++)
	{
		$pembagi[$i] = 0;
		for ($j=0;$j<count($alternatif);$j++)
		{
			$pembagi[$i] = $pembagi[$i] + ($alternatifkriteria[$j][$i] * $alternatifkriteria[$j][$i]);
		}
		$pembagi[$i] = sqrt($pembagi[$i]);
	}
	
	$normalisasi = array();
	
	for ($i=0;$i<count($alternatif);$i++)
	{
		for ($j=0;$j<count($kriteria);$j++)
		{
			$normalisasi[$i][$j] = $alternatifkriteria[$i][$j] / $pembagi[$j];
		}
	}

	$terbobot = array();
	
	for ($i=0;$i<count($alternatif);$i++)
	{
		for ($j=0;$j<count($kriteria);$j++)
		{
			$terbobot[$i][$j] = $normalisasi[$i][$j] * $kepentingan[$j];
		}
	}	
	
	$aplus = array();
	
	for ($i=0;$i<count($kriteria);$i++)
	{
		if ($costbenefit[$i] == 'cost')
		{
			for ($j=0;$j<count($alternatif);$j++)
			{
				if ($j == 0) 
				{ 
					$aplus[$i] = $terbobot[$j][$i];
				}
				else 
				{
					if ($aplus[$i] > $terbobot[$j][$i])
					{
						$aplus[$i] = $terbobot[$j][$i];
					}
				}
			}
		}
		else 
		{
			for ($j=0;$j<count($alternatif);$j++)
			{
				if ($j == 0) 
				{ 
					$aplus[$i] = $terbobot[$j][$i];
				}
				else 
				{
					if ($aplus[$i] < $terbobot[$j][$i])
					{
						$aplus[$i] = $terbobot[$j][$i];
					}
				}
			}
		}
	}
	
	$amin = array();
	
	for ($i=0;$i<count($kriteria);$i++)
	{
		if ($costbenefit[$i] == 'cost')
		{
			for ($j=0;$j<count($alternatif);$j++)
			{
				if ($j == 0) 
				{ 
					$amin[$i] = $terbobot[$j][$i];
				}
				else 
				{
					if ($amin[$i] < $terbobot[$j][$i])
					{
						$amin[$i] = $terbobot[$j][$i];
					}
				}
			}
		}
		else 
		{
			for ($j=0;$j<count($alternatif);$j++)
			{
				if ($j == 0) 
				{ 
					$amin[$i] = $terbobot[$j][$i];
				}
				else 
				{
					if ($amin[$i] > $terbobot[$j][$i])
					{
						$amin[$i] = $terbobot[$j][$i];
					}
				}
			}
		}
	}
	
	$dplus = array();
	
	for ($i=0;$i<count($alternatif);$i++)
	{
		$dplus[$i] = 0;
		for ($j=0;$j<count($kriteria);$j++)
		{
			$dplus[$i] = $dplus[$i] + (($aplus[$j] - $terbobot[$i][$j]) * ($aplus[$j] - $terbobot[$i][$j]));
		}
		$dplus[$i] = sqrt($dplus[$i]);
	}
	
	$dmin = array();
	
	for ($i=0;$i<count($alternatif);$i++)
	{
		$dmin[$i] = 0;
		for ($j=0;$j<count($kriteria);$j++)
		{
			$dmin[$i] = $dmin[$i] + (($terbobot[$i][$j] - $amin[$j]) * ($terbobot[$i][$j] - $amin[$j]));
		}
		$dmin[$i] = sqrt($dmin[$i]);
	}
	
	
	$hasil = array();
	
	for ($i=0;$i<count($alternatif);$i++)
	{
		$hasil[$i] = $dmin[$i] / ($dmin[$i] + $dplus[$i]);
	}	
	
	$alternatifrangking = array();
	$hasilrangking = array();
	
	for ($i=0;$i<count($alternatif);$i++)
	{
		$hasilrangking[$i] = $hasil[$i];
		$alternatifrangking[$i] = $alternatif[$i];
	}
	
	for ($i=0;$i<count($alternatif);$i++)
	{
		for ($j=$i;$j<count($alternatif);$j++)
		{
			if ($hasilrangking[$j] > $hasilrangking[$i])
			{
				$tmphasil = $hasilrangking[$i];
				$tmpalternatif = $alternatifrangking[$i];
				$hasilrangking[$i] = $hasilrangking[$j];
				$alternatifrangking[$i] = $alternatifrangking[$j];
				$hasilrangking[$j] = $tmphasil;
				$alternatifrangking[$j] = $tmpalternatif;
			}
		}
	}
?>
              <br />
 
alternatif =
<?php tampilbaris($alternatif); ?>
<br />
kriteria =
<?php tampilbaris($kriteria); ?>
<br />
costbenefit =
<?php tampilbaris($costbenefit); ?>
<br />
kepentingan =
<?php tampilbaris($kepentingan); ?>
<br />
alternatifkriteria=
<?php tampiltabel($alternatifkriteria); ?>
<br />
pembagi =
<?php tampilbaris($pembagi); ?>
<br />
normalisasi=
<?php tampiltabel($normalisasi); ?>
<br />
terbobot=
<?php tampiltabel($terbobot); ?>
<br />
A+ =
<?php tampilbaris($aplus); ?>
<br />
A- =
<?php tampilbaris($amin); ?>
<br />
D+=
<?php tampilkolom($dplus); ?>
<br />
D-=
<?php tampilkolom($dmin); ?>
<br />
hasil=
<?php tampilkolom($hasil); ?>
<br />
hasilranking=
<?php tampilkolom($hasilrangking); ?>
<br />
alternatifranking=
<?php tampilkolom($alternatifrangking); ?>
<br />
alternatif terbaik = <?php echo $alternatifrangking[0]; ?> dengan nilai terbesar = <?php echo $hasilrangking[0]; ?>
<br /><a href="<?php echo site_url('home'); ?>">Kembali</a>