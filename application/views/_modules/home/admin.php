<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Dashboard <small>Selamat Datang</small>
        </h1>

        <h3 class="text-center">Sistem Pendukung Keputusan Pemberian Dana BP2KP GunungKidul</h3>
        <p class="text-center">
            Selamat datang <strong><?php echo $this->user->username; ?></strong>.
        </p>

        <?php if($this->user->role == 'executive'): ?>
            <p class="text-center">
                <a href="<?php echo site_url('Topsis') ?>" class="btn btn-primary btn-lg">Buka Analisa Topsis</a>
            </p>
        <?php endif; ?>
    </div>
</div>