<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Topsis extends EM_Controller {

	public function __construct() {
		parent::__construct();

		$this->restrict('all');
	}

	public function index()
	{
		$alternatif = array();
	
		$this->load->model('model_alternatif');
		$this->load->model('model_kriteria');
		$this->load->model('model_alternatif_kriteria');

		$tabelalternatif = $this->model_alternatif->getdata();
		$data['tabelalternatif'] = $tabelalternatif;

		$i=0;
		foreach ($tabelalternatif as $rowalternatif) {
			$alternatif[$i] = $rowalternatif->nama_alternatif;
			$deskripsi[$i] = $rowalternatif->deskripsi;

			$i++;
		}
		
		$kriteria = array();
		$costbenefit = array();
		$kepentingan = array(); 

		$tabelkriteria = $this->model_kriteria->getdata();
		$data['tabelkriteria'] = $tabelkriteria;

		$i=0;
		foreach ($tabelkriteria as $rowkriteria) {
			$kriteria[$i] = $rowkriteria->nama_kriteria;
			$costbenefit[$i] = $rowkriteria->costbenefit;
			$kepentingan[$i] = $rowkriteria->kepentingan;

			$i++;
		}
		
		$alternatifkriteria = array();
		
		$i=0;
		foreach ($tabelalternatif as $rowalternatif) {
			$j=0;
			foreach ($tabelkriteria as $rowkriteria) {
				$rowalternatifkriteria = $this->model_alternatif_kriteria
					->select_alternatif_kriteria($rowalternatif->id_alternatif, $rowkriteria->id_kriteria);
				
				$alternatifkriteria[$i][$j] = $rowalternatifkriteria ? $rowalternatifkriteria->nilai : 0;

				$j++;
			}

			$i++;
		}
			
		$pembagi = array();
		
		for ($i=0;$i<count($kriteria);$i++) {
			$pembagi[$i] = 0;
			for ($j=0;$j<count($alternatif);$j++) {
				$pembagi[$i] = $pembagi[$i] + ($alternatifkriteria[$j][$i] * $alternatifkriteria[$j][$i]);
			}
			$pembagi[$i] = sqrt($pembagi[$i]);
		}
		
		$normalisasi = array();
		
		for ($i=0;$i<count($alternatif);$i++) {
			for ($j=0;$j<count($kriteria);$j++) {
				$normalisasi[$i][$j] = $alternatifkriteria[$i][$j] / $pembagi[$j];
			}
		}

		$terbobot = array();
		
		for ($i=0;$i<count($alternatif);$i++) {
			for ($j=0;$j<count($kriteria);$j++) {
				$terbobot[$i][$j] = $normalisasi[$i][$j] * $kepentingan[$j];
			}
		}	
		
		$aplus = array();
		
		for ($i=0;$i<count($kriteria);$i++) {
			if ($costbenefit[$i] == 'cost') {
				for ($j=0;$j<count($alternatif);$j++) {
					if ($j == 0)  { 
						$aplus[$i] = $terbobot[$j][$i];
					}
					else  {
						if ($aplus[$i] > $terbobot[$j][$i]) {
							$aplus[$i] = $terbobot[$j][$i];
						}
					}
				}
			}
			else  {
				for ($j=0;$j<count($alternatif);$j++) {
					if ($j == 0)  { 
						$aplus[$i] = $terbobot[$j][$i];
					}
					else  {
						if ($aplus[$i] < $terbobot[$j][$i]) {
							$aplus[$i] = $terbobot[$j][$i];
						}
					}
				}
			}
		}
		
		$amin = array();
		
		for ($i=0;$i<count($kriteria);$i++) {
			if ($costbenefit[$i] == 'cost') {
				for ($j=0;$j<count($alternatif);$j++) {
					if ($j == 0)  { 
						$amin[$i] = $terbobot[$j][$i];
					}
					else  {
						if ($amin[$i] < $terbobot[$j][$i]) {
							$amin[$i] = $terbobot[$j][$i];
						}
					}
				}
			}
			else  {
				for ($j=0;$j<count($alternatif);$j++) {
					if ($j == 0) { 
						$amin[$i] = $terbobot[$j][$i];
					}
					else {
						if ($amin[$i] > $terbobot[$j][$i]) {
							$amin[$i] = $terbobot[$j][$i];
						}
					}
				}
			}
		}
		
		$dplus = array();
		
		for ($i=0;$i<count($alternatif);$i++) {
			$dplus[$i] = 0;

			for ($j=0;$j<count($kriteria);$j++) {
				$dplus[$i] = $dplus[$i] + (($aplus[$j] - $terbobot[$i][$j]) * ($aplus[$j] - $terbobot[$i][$j]));
			}

			$dplus[$i] = sqrt($dplus[$i]);
		}
		
		$dmin = array();
		
		for ($i=0;$i<count($alternatif);$i++) {
			$dmin[$i] = 0;

			for ($j=0;$j<count($kriteria);$j++) {
				$dmin[$i] = $dmin[$i] + (($terbobot[$i][$j] - $amin[$j]) * ($terbobot[$i][$j] - $amin[$j]));
			}

			$dmin[$i] = sqrt($dmin[$i]);
		}
		
		$hasil = array();
		
		for ($i=0;$i<count($alternatif);$i++) {
			$hasil[$i] = $dmin[$i] / ($dmin[$i] + $dplus[$i]);
		}	
		
		$alternatifrangking = array();
		$hasilrangking = array();
		
		for ($i=0;$i<count($alternatif);$i++) {
			$hasilrangking[$i] = $hasil[$i];
			$alternatifrangking[$i] = $alternatif[$i];
		}
		
		for ($i=0;$i<count($alternatif);$i++) {
			for ($j=$i;$j<count($alternatif);$j++) {
				if ($hasilrangking[$j] > $hasilrangking[$i]) {
					$tmphasil = $hasilrangking[$i];
					$tmpalternatif = $alternatifrangking[$i];
					$hasilrangking[$i] = $hasilrangking[$j];
					$alternatifrangking[$i] = $alternatifrangking[$j];
					$hasilrangking[$j] = $tmphasil;
					$alternatifrangking[$j] = $tmpalternatif;
				}
			}
		}

		$data['alternatif'] = $alternatif;
		$data['deskripsi'] = $deskripsi;
		$data['kriteria'] = $kriteria;
		$data['costbenefit'] = $costbenefit;
		$data['kepentingan'] = $kepentingan;
		$data['alternatifkriteria'] = $alternatifkriteria;
		$data['pembagi'] = $pembagi; 
		$data['normalisasi'] = $normalisasi; 
		$data['terbobot'] = $terbobot; 
		$data['aplus'] = $aplus; 
		$data['amin'] = $amin; 
		$data['dplus'] = $dplus; 
		$data['dmin'] = $dmin; 
		$data['hasil'] = $hasil; 
		$data['hasilrangking'] = $hasilrangking; 
		$data['alternatifrangking'] = $alternatifrangking;

		$this->render('topsis/index', $data); 
	}	

	// private function _tampiltabel($arr)
	// {
	// 	$tabel = "";
	// 	$tabel .= '<table width="500" border="0" cellspacing="1" cellpadding="3" bgcolor="#000099">';
	// 	  for ($i=0;$i<count($arr);$i++)
	// 	  {
	// 	  $tabel .= '<tr>';
	// 		  for ($j=0;$j<count($arr[$i]);$j++)
	// 		  {
	// 		    $tabel .= '<td bgcolor="#FFFFFF">'.$arr[$i][$j].'</td>';
	// 		  }
	// 	  $tabel .= '</tr>';
	// 	  }
	// 	$tabel .= '</table>';
	// 	return $tabel;
	// }

	// private function _tampilbaris($arr)
	// {
	// 	$tabel = "";
	// 	$tabel .= '<table width="500" border="0" cellspacing="1" cellpadding="3" bgcolor="#000099">';
	// 	$tabel .= '<tr>';
	// 		  for ($i=0;$i<count($arr);$i++)
	// 		  {
	// 		    $tabel .= '<td bgcolor="#FFFFFF">'.$arr[$i].'</td>';
	// 		  }
	// 	$tabel .= "</tr>";
	// 	$tabel .= '</table>';
	// 	return $tabel;
	// }

	// private function _tampilkolom($arr)
	// {
	// 	$tabel = "";
	// 	$tabel .= '<table width="500" border="0" cellspacing="1" cellpadding="3" bgcolor="#000099">';
	//   for ($i=0;$i<count($arr);$i++)
	//   {
	// 		$tabel .= '<tr>';
	// 		    $tabel .= '<td bgcolor="#FFFFFF">'.$arr[$i].'</td>';
	// 		$tabel .= "</tr>";
	//    }
	// 	$tabel .= '</table>';
	// 	return $tabel;
	// }
}