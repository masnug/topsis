<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kriteria extends CI_Model {

	public function getdata($offset = false)
	{
		$this->db->from('kriteria');

		if ($offset !== false) {
			if ($offset > 0) {
				$this->db->limit($offset, 20);
			}
			else {
				$this->db->limit(20);
			}
		}

		return $this->db->get()->result();
	}

	public function countdata()
	{
		return $this->db->from('kriteria')->count_all_results();
	}
	
	public function insertdata($data)
	{
		return $this->db->insert('kriteria', $data);
	}
	
	public function deletedata($id)
	{
		$this->db->where('id_kriteria', $id);
		return $this->db->delete('kriteria');
	}

	public function selectdata($id)
	{
		$this->db->where('id_kriteria', $id);
		return $this->db->get('kriteria')->row();
	}
	
	public function updatedata($data, $id)
	{
		$this->db->where('id_kriteria', $id);
		return $this->db->update('kriteria', $data);
	}

}
