<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>SPK Metode Topsis</title>
  <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url('assets/css/custom-styles.css') ?>" rel="stylesheet">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
      <div class="navbar-header">
        <button class="navbar-toggle" data-target=".sidebar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="<?php echo site_url('home') ?>">SPK Topsis</a>
      </div>
    </nav><!--/. NAV TOP  -->
    <div id="page-inner">
      <?php $message = @$this->session->flashdata('message'); ?>

      <?php if ($message): ?>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
              <div class="alert alert-danger">
                <?php echo $message ?>
              </div>
            </div>
          </div>  
      <?php endif ?>

      <?php $this->load->view($page_view); ?>

      <footer>
        <p>All right reserved. Template by: <a href="http://webthemez.com">WebThemez</a></p>
      </footer>
    </div><!-- /. PAGE INNER  -->
  </div><!-- /. WRAPPER  -->
  <!-- JS Scripts-->
  <script src="<?php echo base_url('assets/js/jquery-1.10.2.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script> 
  <script src="<?php echo base_url('assets/js/dataTables/jquery.dataTables.js') ?>"></script> 
  <script src="<?php echo base_url('assets/js/dataTables/dataTables.bootstrap.js') ?>"></script> 
  <script src="<?php echo base_url('assets/js/app.js') ?>"></script>
</body>
</html>